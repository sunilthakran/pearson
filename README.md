Environment Prerequisites:
    * Ruby
    * Compass
    * Grunt CLI

Please make sure you have grunt cli installed via npm:

	npm install -g grunt-cli

Then run 'bower install' and 'npm install' for dependencies.

Finally run: grunt serve from terminal/command line.