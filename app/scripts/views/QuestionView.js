/*global Pearson, Backbone, JST*/

Pearson.Views = Pearson.Views || {};

(function () {
    'use strict';

    Pearson.Views.QuestionView = Backbone.View.extend({

        el: '.page',

        template: JST['app/scripts/templates/QuestionView.ejs'],

        events: {
            'change input[type=radio]': 'changedRadio'
        },

        changedRadio: function (e) {
            $('.next').removeAttr('disabled');

            var answers = this.model.get("answers");

            _.each(answers, function (answer) {
                if ( answer.id == $(e.currentTarget).val() ) {
                    answer.selected = true;
                } else {
                    answer.selected = false;
                }
            });

            this.model.set({ "answers": answers }, {silent:true});

        },

        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
        }

    });

})();
