/*global Pearson, Backbone, JST*/

Pearson.Views = Pearson.Views || {};

(function () {
    'use strict';

    Pearson.Views.ThanksView = Backbone.View.extend({

        el: '.page',

        template: JST['app/scripts/templates/ThanksView.ejs'],

        events: {
            'click .submit': 'submit'
        },

        submit: function (e) {
            this.model.save();
            $('.submit').prop('disabled', true);
        },

        init: function () {
            this.render();
            var answers = this.model.get("answers");

            _.each(answers, function (answer) {
                if ( answer.selected == true ) {
                    $('.submit').prop('disabled', false);
                }
            });
        },

        render: function () {
            this.$el.html(this.template());
        }

    });

})();
