/*global Pearson, Backbone, JST*/

Pearson.Views = Pearson.Views || {};

(function () {
    'use strict';

    Pearson.Views.WelcomeView = Backbone.View.extend({

        el: '.page',

        template: JST['app/scripts/templates/WelcomeView.ejs'],

        render: function () {
            this.$el.html(this.template());
        }

    });

})();
