/*global Pearson, $*/


window.Pearson = {
    Models: {},
    Collections: {},
    Views: {},
    Routers: {},
    init: function () {
        'use strict';
        // instantiate model
        var appModel = new this.Models.AppModel();

        // instantiate views
        var welcomeView = new this.Views.WelcomeView();
        var questionView = new this.Views.QuestionView({
            model: appModel
        });
        var thanksView = new this.Views.ThanksView({
            model: appModel
        });

        // instantiate router
        var router = new this.Routers.AppRouter();

        router.on('route:welcome', function () {
            welcomeView.render();
        });

        router.on('route:question', function () {
            questionView.render();
        });

        router.on('route:thanks', function () {
            thanksView.init();
        });

        Backbone.history.start();
    }
};

$(document).ready(function () {
    'use strict';
    Pearson.init();
});
