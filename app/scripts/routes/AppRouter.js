/*global Pearson, Backbone*/

Pearson.Routers = Pearson.Routers || {};

(function () {
    'use strict';

    Pearson.Routers.AppRouter = Backbone.Router.extend({
    	routes: {
    		"": "welcome",
			"question": "question",
			"thanks":  "thanks"
		}
    });

})();
