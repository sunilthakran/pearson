/*global Pearson, Backbone*/

Pearson.Models = Pearson.Models || {};

(function () {
    'use strict';

    Pearson.Models.AppModel = Backbone.Model.extend({

        urlRoot: '/',

        defaults: {
            question: 'What is the capital of Switzerland?',
            answers: [
                { 'id': 1, 'value': 'Bern', 'selected': false },
                { 'id': 2, 'value': 'Zurich', 'selected': false },
                { 'id': 3, 'value': 'London', 'selected': false },
                { 'id': 4, 'value': 'Paris', 'selected': false }
            ]
        }

    });

})();
